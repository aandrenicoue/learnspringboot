package com.nicoue.expense.Service;

import com.nicoue.expense.Model.Employee;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


public interface EmployeeService {
    List<Employee> getEmployees(int pageNumber,int pageSize);
    Employee saveEmployee(Employee employee);

    Employee getEmployeeDetails(long id);

    Employee updateEmployee(Employee employee);
    void  deleteEmployee (long id);

    List<Employee> getEmployeesByName(String name);
    List<Employee> getEmployeesByKeyword(String name);

    List<Employee> getEmployeesByNameOrLocation(String name,String location);

}
