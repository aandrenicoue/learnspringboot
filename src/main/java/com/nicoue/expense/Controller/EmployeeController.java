package com.nicoue.expense.Controller;

import com.nicoue.expense.Model.Department;
import com.nicoue.expense.Model.Employee;
import com.nicoue.expense.Repository.DepartmentRepository;
import com.nicoue.expense.Repository.EmployeeRepository;
import com.nicoue.expense.Request.EmployeeRequest;
import com.nicoue.expense.Service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EmployeeController {

    //Injection of dependence of service.
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private DepartmentRepository departmentRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    //localhost:8000/employees
    @GetMapping("employees")
    public ResponseEntity<List<Employee>> getEmployees(@RequestParam Integer pageNumber, @RequestParam Integer pageSize) {
    //Pagination and sort
        return new ResponseEntity<List<Employee>>(employeeService.getEmployees(pageNumber, pageSize), HttpStatus.OK);
    }

    //localhost:8000/employee/1
    @GetMapping("employee/{id}")
    public ResponseEntity<Employee> getEmployeeDetails(@PathVariable("id") Long id) {
        return new ResponseEntity<Employee>(employeeService.getEmployeeDetails(id), HttpStatus.OK);
    }

    @PostMapping("employee")
    public ResponseEntity<Employee> saveEmployee(@Valid @RequestBody EmployeeRequest request) {

//
//        Department department = new Department();
//        department.setName(request.getDepartment());
//        department = departmentRepository.save(department);

        Employee employee = new Employee(request);
        //employee.setDepartment(department);
        employeeRepository.save(employee);
        return new ResponseEntity<Employee>(employee, HttpStatus.CREATED);
        // return new ResponseEntity<Employee>(employeeService.saveEmployee(employee),HttpStatus.CREATED) ;
    }

    //localhost:8000/employee/1
    @PutMapping("employee/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable("id") Long id, @Valid @RequestBody Employee employee) {
        employee.setId(id);
        return new ResponseEntity<Employee>(employeeService.updateEmployee(employee), HttpStatus.OK);
    }

    //localhost:8000/employee?id=1
    @DeleteMapping("employee")
    public ResponseEntity<HttpStatus> deleteEmployee(@RequestParam("id") Long id) {
        employeeService.deleteEmployee(id);
        return new ResponseEntity<HttpStatus>(HttpStatus.NO_CONTENT);
    }

    //localhost:8000/employees/filterByNameOrLocation
    @GetMapping("employees/filterByNameOrLocation")
    public ResponseEntity<List<Employee>> getEmployeesByNameOrLocation(@RequestParam String name, @RequestParam String location) {
        return new ResponseEntity<List<Employee>>(employeeService.getEmployeesByNameOrLocation(name, location), HttpStatus.OK);
    }

    //localhost:8000/employees
    @GetMapping("employees/filterByName")
    public ResponseEntity<List<Employee>> getEmployeesByName(@RequestParam String name) {
        return new ResponseEntity<List<Employee>>(employeeService.getEmployeesByName(name), HttpStatus.OK);
    }

    //localhost:8000/employees/sortByName
    @GetMapping("employees/sortByName")
    public ResponseEntity<List<Employee>> sortEmployeesByName(@RequestParam String name) {
        return new ResponseEntity<List<Employee>>(employeeService.getEmployeesByKeyword(name), HttpStatus.OK);
    }

    //localhost:8000/employees/department
//    @GetMapping("employees/department")
//    public ResponseEntity<List<Employee>> getEmployeeByDepartment(@RequestParam String name) {
//        return new ResponseEntity<List<Employee>>(employeeRepository.findByDepartmentName(name), HttpStatus.OK);
//    }

}
