package com.nicoue.expense.Model;

import com.nicoue.expense.Request.EmployeeRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Setter
@Getter
@ToString
@Entity
@Table(name = "tbl_employee")
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;

    @NotBlank(message = "Name should not be null")
    private String name;

    private Long age=0L;

    @NotBlank
    private String location;

    @NotBlank
    @Email(message = "Please enter the valid email address")
    private String email;



    @CreationTimestamp
    @Column(name = "created_at",nullable = false,updatable = false)
    private Date createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    private Date updatedAt;

    public Employee(EmployeeRequest request) {
        this.name = request.getName();
        this.age = request.getAge();
        this.email = request.getEmail();
        this.location = request.getLocation();
    }

//    @OneToOne
//    @JoinColumn(name = "department_id")
//    private  Department department;
}
