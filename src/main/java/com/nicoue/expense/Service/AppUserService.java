package com.nicoue.expense.Service;

import com.nicoue.expense.Model.AppUser;
import com.nicoue.expense.Model.Role;

import java.util.List;

public interface AppUserService {

    AppUser saveUser(AppUser user);

    void addRoleToUser(String username,String roleName);

    AppUser getUser(String username);

    List<AppUser> getUsers();
}
