package com.nicoue.expense.Request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class EmployeeRequest {
    private String name;
    private Long age=0L;
    private String location;
    private String email;
    private String department;

}
