package com.nicoue.expense.Repository;

import com.nicoue.expense.Model.Employee;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
//JpaRepository we won't want to use pagination
//PagingAndSortingRepository in Pagination case
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Long> {
    List<Employee> findByName(String name);

    //List<Employee> findByDepartmentName(String name);

    //Where multiple
    List<Employee> findByNameAndLocation(String name,String location);

    //Like methode
    List<Employee> findByNameContaining(String keyword, Sort sort);

    @Query("FROM Employee Where name= :name Or location = :location")
    List<Employee> getEmployeeByNameOrLocation(String name,String location);


}
