package com.nicoue.expense.Service;

import com.nicoue.expense.Model.Role;

public interface RoleService {
    Role saveRole(Role role);
}
