package com.nicoue.expense.Repository;

import com.nicoue.expense.Model.Department;
import com.nicoue.expense.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Repository
 public interface DepartmentRepository extends JpaRepository<Department,Long> {


}
